const navItems = document.querySelectorAll('.navbar__item')

const delNavItemActive = () => {
    navItems.forEach(navItem => {
        navItem.classList.remove('active')
    });
}

navItems.forEach(navItem => {
    navItem.addEventListener('click', (e) => {
        delNavItemActive()
        navItem.classList.toggle('active')
    })
});

const navTogglers = document.querySelectorAll('.navbar__toggler')
const sideNav = document.getElementById('side-nav')

navTogglers.forEach(navToggler => {
    navToggler.addEventListener('click', (e) => {
        sideNav.classList.toggle('active')
    })
});