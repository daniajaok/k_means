const downloadHTML = document.getElementById('download-html')
const downloadEXCEL = document.getElementById('download-excel')
const loadingWrapper = document.getElementById('loading-wrapper')

function loadContent(url){
    var xhr = new XMLHttpRequest();
    xhr.responseType = 'blob'
    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE) { // XMLHttpRequest.DONE == 4
            if (xhr.status == 200) {
                // Get File Name
                var disposition = xhr.getResponseHeader('Content-Disposition');
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(xhr.response);
                a.href = url;
                a.download = matches[1] ? matches[1] : 'Report.tar.gz';
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
            }
            else if (xhr.status == 400) {
                alert('Failed to download file');
            }
            loadingWrapper.classList.remove('active');
        }
    };
    xhr.open("GET", url, true);
    xhr.send();
}

downloadHTML.addEventListener('click', (e) => {
    e.preventDefault();
    loadingWrapper.classList.add('active');
    var url = "/download-html";
    loadContent(url);
})
downloadEXCEL.addEventListener('click', (e) => {
    e.preventDefault();
    loadingWrapper.classList.add('active');
    var url = "/download-excel";
    loadContent(url);
})