# Aplikasi Analisis sebaran COVID-19, Mengunkan metode k_means di daerah Tasik

Dalam studi kasus analisis sebaran COVID-19 menggunakan aplikasi berbasis Django dengan metode k-means, kami menggunakan metrik evaluasi seperti Silhouette Score dan elbow untuk memahami pola sebaran virus.

Silhouette Score memberikan informasi tentang seberapa baik setiap titik data ditempatkan dalam klasternya masing-masing, dengan nilai semakin mendekati 1 menandakan klaster yang lebih baik terdefinisi. Kami menggunakan nilai Silhouette Score untuk mengevaluasi seberapa baik pembagian klaster kami dalam mengelompokkan wilayah-wilayah dengan tingkat infeksi yang serupa.

Selain itu, kami juga menerapkan metode elbow untuk menentukan jumlah optimal dari klaster-klasternya. Grafik elbow memungkinkan kami untuk mengidentifikasi titik di mana penambahan klaster tidak memberikan peningkatan yang signifikan dalam penjelasan variasi data. Ini membantu kami dalam menentukan jumlah klaster yang optimal untuk menganalisis sebaran COVID-19.

Dengan menggunakan Django sebagai platform aplikasi, kami dapat membangun antarmuka yang interaktif dan responsif untuk memvisualisasikan hasil analisis k-means ini kepada pengguna dengan cara yang mudah dipahami. Ini memungkinkan pengguna untuk menjelajahi sebaran COVID-19 secara lebih terperinci dan memperoleh wawasan yang berharga untuk pengambilan keputusan dan penanganan pandemi.

### Note 
jika anda ingin mejalankanya aplikasi tolong hubingi saya [Muhammad Ramdhani](wa.me/+6281399057525) karena aplikasi ini ada data privasi yang tidak saya sertakan pada repository ini