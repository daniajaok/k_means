from django.contrib import admin
from django.urls import path
from . import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('real-data', views.real_data, name='real_data'),
    path('uncleaned-data', views.uncleaned_data, name='uncleaned_data'),
    path('cleaned-data', views.cleaned_data, name='cleaned_data'),
    path('download-html', views.download_html, name='download_html'),
    path('download-excel', views.download_excel, name='download_excel'),
    path('download-and-show-data-cluster', views.download_and_show_data_cluster, name='download_and_show_data_cluster'),
]
