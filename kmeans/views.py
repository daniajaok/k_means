from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
from pandas_profiling import ProfileReport
import tarfile
import pandas as pd
import json


print('Starting Data Preparation ...')

# Data Web
DATA_WEB = {
    'upload_file': False
}

# Received Data
data_diterima = pd.read_excel (settings.BASE_DIR / 'static/dataset/__DATA COVID 2020.xlsx')

# Uncleanup Data
df = pd.read_excel (settings.BASE_DIR / 'static/dataset/__DATA_COVID_2020_v1B.xlsx')
df.dropna()
df['simptomatik'].replace({True:1,False:0},inplace=True)
df['simptomatik'].map({True: 1, False: 0})  
df['Asimptomatik'].replace({True:1,False:0},inplace=True)
df['BULAN_POSITIF'] = pd.DatetimeIndex(df['TANGGAL_POSITIF']).month
df['Tanggal_POSITIF'] = pd.DatetimeIndex(df['TANGGAL_POSITIF']).day
df['BULAN_SEMBUH'] = pd.DatetimeIndex(df['TANGGAL_SEMBUH']).month
df['Tanggal_SEMBUH'] = pd.DatetimeIndex(df['TANGGAL_SEMBUH']).day
df.fillna(0)
df1=df.copy()
df1=df1.drop(['TANGGAL_POSITIF','TANGGAL_SEMBUH'], axis = 1)

# Cleanup Data
df2 = df1.copy()
def remove_outlier(df):
    Q1=df.quantile(0.25)
    Q3=df.quantile(0.75)
    IQR=Q3-Q1
    df_final=df[~(df>(Q1-(1.5*IQR)))|(df<(Q3+(1.5*IQR)))]
    return df_final

for x in range(2):
    df2=remove_outlier(df2)
    df2.dropna(axis=0,inplace=True)

df5 = df2.copy()

df3= pd.read_excel(settings.BASE_DIR / 'static/dataset/K-Means.xlsx')
df3.drop(['simptomatik','Asimptomatik','Tanggal_POSITIF','Tanggal_SEMBUH'],axis=1, inplace=True)
df4=df3.copy()
df4['Gejala'] = df4['Gejala'].astype(int)

df3["UMUR"] = (df3["UMUR"] - df3["UMUR"].min()) / (df3["UMUR"].max() -df3["UMUR"].min())
df3["JENIS_KELAMIN"] = (df3["JENIS_KELAMIN"] - df3["JENIS_KELAMIN"].min()) / (df3["JENIS_KELAMIN"].max() - df3["JENIS_KELAMIN"].min())
df3["Kode Kelurahan"] = (df3["Kode Kelurahan"] - df3["Kode Kelurahan"].min()) / (df3["Kode Kelurahan"].max() - df3["Kode Kelurahan"].min())
df3["BULAN_POSITIF"] = (df3["BULAN_POSITIF"] - df3["BULAN_POSITIF"].min()) / (df3["BULAN_POSITIF"].max() - df3["BULAN_POSITIF"].min())
df3["BULAN_SEMBUH"] = (df3["BULAN_SEMBUH"] - df3["BULAN_SEMBUH"].min()) / (df3["BULAN_SEMBUH"].max() - df3["BULAN_SEMBUH"].min())
df3["Gejala"] = (df3["Gejala"] - df3["Gejala"].min()) / (df3["Gejala"].max() - df3["Gejala"].min())

print('Completed Data Preparation ...')


def index(request):
    context = {
        'title_page': 'Upload File',
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'upload_file.html', context)

def real_data(request):
    if request.GET.get('upload_file', False):
        DATA_WEB['upload_file'] = True
    title_data = data_diterima.columns
    json_records = data_diterima.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title': 'K-Means | Data Received From The Service',
        'title_page': 'Data Received From The Service',
        'title_data': title_data,
        'content_data': data_csv,
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context=context)

def uncleaned_data(request):
    title_data = df1.columns
    json_records = df1.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title': 'K-Means | Data Before Cleanup',
        'title_page': 'Data Before Cleanup',
        'title_data': title_data,
        'content_data': data_csv,
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context=context)

def cleaned_data(request):
    if 'Hasil_KMeans' in df2.columns:
        df2.drop(['Hasil_KMeans'], axis=1, inplace=True)
    if 'Gejala' in df2.columns:
        df2.drop(['Gejala'], axis=1, inplace=True)

    cluster_view = False
    if request.GET.get('refresh_data', False):
        sc = StandardScaler()
        df_std = sc.fit_transform(df3)
        kmeans1b = KMeans(n_clusters=4, random_state=42).fit(df_std)
        labels1b = kmeans1b.labels_

        df2['Hasil_KMeans']=labels1b
        df2['Gejala']=df4['Gejala']
        
        df5['Cluster']=labels1b
        df5['Gejala']=df4['Gejala']  
        
        cluster_view = True

    title_data = df2.columns
    json_records = df2.reset_index().to_json(orient='records')
    data_csv = json.loads(json_records)
    context = {
        'title': 'K-Means | Data After Cleanup',
        'title_page': 'Data After Cleanup',
        'refresh_button': True,
        'title_data': title_data,
        'content_data': data_csv,
        'cluster_view': cluster_view,
        'upload_file': DATA_WEB['upload_file'],
    }
    return render(request, 'index.html', context=context)

def download_html(request):
    profile = ProfileReport(df2, title="k means clustering")
    profile.to_file("k_means_clustering.html")
    response = HttpResponse(content_type='text/html')
    response['Content-Disposition'] = 'attachment; filename=k_means_clustering.tar.gz'
    tarred = tarfile.open(fileobj=response, mode='w:gz')
    tarred.add('k_means_clustering.html')
    tarred.close()
    return response

def download_excel(request):
    df2.to_excel("KMeansCovid.xlsx", index=False)
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=KMeansCovid.tar.gz'
    tarred = tarfile.open(fileobj=response, mode='w:gz')
    tarred.add('KMeansCovid.xlsx')
    tarred.close()
    return response

def download_and_show_data_cluster(request):
    cluster = request.GET.get('cluster', None)
    download_cluster = request.GET.get('download', False)
    data_cluster = df1.copy()
    sub_title = 'Cluster'
    data_query_param = cluster
    cluster_view = True
    title_data_cluster = ''

    if cluster == 'cluster0':
        title_data_cluster = 'SpectraClastering0'
        sub_title = 'Cluster 0'
        data_cluster = df5[df5['Cluster'] == 0]
    elif cluster == 'cluster1':
        title_data_cluster = 'SpectraClastering1'
        sub_title = 'Cluster 1'
        data_cluster = df5[df5['Cluster'] == 1]
    elif cluster == 'cluster2':
        title_data_cluster = 'SpectraClastering2'
        sub_title = 'Cluster 2'
        data_cluster = df5[df5['Cluster'] == 2]
    elif cluster == 'cluster3':
        title_data_cluster = 'SpectraClastering3'
        sub_title = 'Cluster 3'
        data_cluster = df5[df5['Cluster'] == 3]


    if download_cluster == '1': # Download data cluster
        data_cluster.to_excel("%s.xlsx" % (title_data_cluster))
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename=%s.tar.gz' % (title_data_cluster)
        tarred = tarfile.open(fileobj=response, mode='w:gz')
        tarred.add('%s.xlsx' % (title_data_cluster))
        tarred.close()
        return response
    else: # View data cluster
        title_data = data_cluster.columns
        json_records = data_cluster.reset_index().to_json(orient='records')
        data_csv = json.loads(json_records)
        context = {
            'title': 'Spectra Clastering | %s' % (sub_title),
            'refresh_cluster_button': True,
            'title_page': sub_title,
            'title_data': title_data,
            'content_data': data_csv,
            'cluster_view': cluster_view,
            'data_query_param': data_query_param,
            'upload_file': DATA_WEB['upload_file'],
        }
        return render(request, 'index.html', context=context)